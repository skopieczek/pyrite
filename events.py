from collections import defaultdict


class Event(object):
    """An Event represents a change of state of kind within the game world."""
    def __init__(self, raiser):
        self.raiser = raiser

    def __repr__(self):
        return "<%s: raised by %s>" % (self.__class__.__name__,
                                       str(self.raiser))


class TimerTickEvent(Event):
    def __init__(self):
        super(TimerTickEvent, self).__init__(None)

    def __repr__(self):
        return "<TimerTickEvent>"


class EventProxy(object):
    """The EventProxy represents anything responsible for aggregating events
       and passing them on to listeners.
       This allows the hierarchy-navigating code to be kept common."""
    def __init__(self, initially_active=False):
        self._listeners = defaultdict(list)
        self._is_active = initially_active

    def setActive(self, is_active):
        self._is_active = is_active

    def addListener(self, handler, event_class):
        self._listeners[event_class].append(handler)

    def removeListener(self, handler, event_class):
        self._listeners[event_class].remove(handler)

    def dispatchEvent(self, event):
        for event_class in _event_hierarchy_walk(event.__class__):
            for handler in self._listeners[event_class]:
                handler(event)


class EventGenerator(object):
    """An EventGenerator is any object which binds to an EventProxy and passes
       it events relating to itself."""
    def __init__(self):
        self._proxy = None

    def bind(self, proxy):
        self._proxy = proxy

    def clear(self):
        self._proxy = None

    def fireEvent(self, event):
        if hasattr(self, "_proxy") and self._proxy is not None:
            self._proxy.dispatchEvent(event)


def _event_hierarchy_walk(event_class):
    while event_class != Event.__bases__[0]:
        yield event_class
        event_class = event_class.__bases__[0]
