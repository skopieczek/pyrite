import itertools
import re
from functools import partial

import things
import sentenceutils

_TEXT_VARIABLE_RE = re.compile(r'\(([!?]*)([a-z]+)\)')
_THING_VARIABLE_RE = re.compile(r'<([!?]*)([a-z]+)>')


class ParserContext(object):
    def getEntities(self):
        return []  # Override

    def hasEntity(self, entity_identifier):
        return False  # Override

    def getEntity(self, entity_identifier):
        return None  # Override


class ChainedContext(ParserContext):
    def __init__(self, *contexts):
        super(ChainedContext, self).__init__()
        self._contexts = contexts

    def getEntities(self):
        return list(itertools.chain(*map(ParserContext.getEntities,
                                         self._contexts)))

    def hasEntity(self, entity_identifier):
        return any(map(partial(ParserContext.hasEntity,
                               entity_identifier=entity_identifier),
                       self._contexts))

    def getEntity(self, entity_identifier):
        for context in self._contexts:
            entity = context.getEntity(entity_identifier)
            if entity is not None:
                return entity
        return None


def parse_with_pattern(text, pattern, definitions, context):
    pattern_tokens = tokenize(pattern, definitions)
    possibilities = []
    for partitioning in partition_text(text, pattern_tokens):
        match = True
        result = {}
        for variable, words in partitioning:
            if not match:
                break
            if isinstance(variable, TextVariable):
                if len(words) > 0:
                    result[variable.name] = words
                elif not variable.optional:
                    match = False
            else:
                for thing, parent in (
                    itertools.izip(_hierarchy(words),
                                   itertools.chain(
                                       [None],
                                       _hierarchy(words)))):
                    thing_entity = context.getEntity(thing)
                    if thing_entity is None:
                        match = False
                        break
                    if parent is not None:
                        parent_entity = context.getEntity(parent)
                        if ((not isinstance(parent_entity,
                                            things.Container)) or
                                thing_entity not in parent_entity):
                            match = False
                            break
                result[variable.name] = thing_entity

        if match:
            possibilities.append(result)

    # Remove matches with long blocks of text variables; consider what might
    # happen to 'laugh at jim merrily' with pattern
    # "laugh <!?object> (!?adverb)".
    if len(possibilities) > 1:
        possibilities_with_penalties = [
            (possibility, _calculate_penalty(possibility))
            for possibility in possibilities]
        min_penalty = min(
            (penalty for possibility, penalty in possibilities_with_penalties))

        return [possibility for possibility, penalty
                in possibilities_with_penalties
                if penalty == min_penalty]
    else:
        return possibilities


def _calculate_penalty(possibility):
    """The penalty for a possible interpretation of a command is given by the
       sum of the squares of the lengths of its text variables.
       This penalises interpretations where the text variable acts 'greedily'
       and captures all of the words in the command, leaving a possible, but
       probably underspecified, interpretation of the command:
        Example: "laugh at jim merrily" should not capture 'at jim merrily' in
        the adverbal phrase at the end of the word."""
    penalty = 0
    for variable in possibility:
        if isinstance(variable, TextVariable):
            penalty += len(possibility[variable]) * len(possibility[variable])
    return penalty


def _hierarchy(words):
    words = words.split(' ')
    current_object_identifier = ""
    for word in words[::-1]:
        if word == "in":
            yield current_object_identifier
            current_object_identifier = ""
        else:
            current_object_identifier = word + " " + current_object_identifier
    yield current_object_identifier


def tokenize(pattern_string, definitions):
    token_strings = pattern_string.lower().split()

    tokens = []
    current_variables = []
    for token_string in token_strings:
        match = _THING_VARIABLE_RE.match(token_string)
        variable = None
        if match is not None:
            variable = ThingVariable(match.group(2))
        else:
            match = _TEXT_VARIABLE_RE.match(token_string)
            if match is not None:
                variable = TextVariable(match.group(2))

        if variable is not None:
            # We matched a variable. Store it off.
            modifiers = match.group(1)
            variable_name = match.group(2)
            if '!' in modifiers:
                variable.capturing = True
            if '?' in modifiers:
                variable.optional = True
            if variable_name in definitions:
                variable.predicate = definitions[variable_name]
            current_variables.append(variable)
        else:
            # We matched a literal, so can complete the token.
            # Store it and reset the token builder.
            literal = Literal(token_string)
            token = {'terminal_literal': literal,
                     'variables': current_variables}
            tokens.append(token)
            current_variables = []

    token = {'terminal_literal': None,
             'variables': current_variables}
    tokens.append(token)

    return tokens


def partition_text(text, pattern_tokens):
    words = sentenceutils.words(text)
    for apportionment in _apportion_words(words, pattern_tokens):
        result = []
        for subblock in apportionment:
            result.extend(subblock)
        yield result


def _apportion_words(words, token_blocks):
    if len(token_blocks) == 1:
        for apportionment in _apportion_words_in_block(words, token_blocks[0]):
            yield (apportionment,)
        raise StopIteration()

    token_block = token_blocks[0]
    literal = token_block['terminal_literal']
#    if len(token_block['variables']) == 0:
#        # TODO Ensure first word was right
#        for apportionment in _apportion_words(words[1:], token_blocks[1:]):
#            yield apportionment
#            raise StopIteration()

    for idx in (idx for idx, word in enumerate(words)
                if literal is None or word == literal.contents):
        for subsplit in _apportion_words_in_block(words[:idx+1], token_block):
            for apportionment in _apportion_words(words[idx+1:],
                                                  token_blocks[1:]):
                yield (subsplit,) + apportionment


def _apportion_words_in_block(words, token_block):
    literal = token_block['terminal_literal']
    if literal is not None:
        if words[-1] != literal.contents:
            raise StopIteration()
        else:
            words = words[:-1]

    return _apportion_words_to_variables(words, token_block['variables'])


def _apportion_words_to_variables(words, variables):
    if len(variables) == 1:
        yield ((variables[0], ' '.join(words)),)
        raise StopIteration()
    elif len(variables) == 0:
        if len(words) == 0:
            yield tuple()
        raise StopIteration()

    variable = variables[0]
    startidx = 0 if variable.optional else 1
    for idx in xrange(startidx, len(words)):
        for apportionment in _apportion_words_to_variables(words[idx:],
                                                           variables[1:]):
            yield ((variable, words[:idx]),) + apportionment


class Literal(object):
    def __init__(self, contents):
        self.contents = contents

    def __repr__(self):
        return "<Literal: %s>" % self.contents


class Variable(object):
    def __init__(self, name, predicate=None, capturing=False, optional=False):
        self.name = name
        self.predicate = predicate
        self.capturing = capturing
        self.optional = optional


class ThingVariable(Variable):
    def __repr__(self):
        return "<Variable (Thing): %s>" % self.name


class TextVariable(Variable):
    def __repr__(self):
        return "<Variable (Text): %s>" % self.name
