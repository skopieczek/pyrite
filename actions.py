import commandparser


class Action(object):
    syntax = []

    def __init__(self, viewport, context, actor, **kwargs):
        self._viewport = viewport
        self._context = context
        self._actor = actor
        self._arguments = kwargs

    def __str__(self):
        return "<Action (%s): actor='%s', context='%s', args='%s'>" % (
            str(type(self)), str(self._actor), str(self._context),
            str(self._arguments))

    def __call__(self):
        pass

    def describe(self):
        pass

    @classmethod
    def parse(clazz, text, viewport, context, actor):
        matches = []
        for priority_group in clazz.syntax:
            for pattern, definitions in priority_group:
                interpretations = commandparser.parse_with_pattern(text,
                                                                   pattern,
                                                                   definitions,
                                                                   context)
                matches.extend(interpretations)

            if len(matches) > 0:
                break

        interpretations = []
        for match in matches:
            interpretations.append(clazz(viewport, context, actor, **match))

        return interpretations


class Look(Action):
    syntax = [[("look", {}),
               ("look <!entity>", {})],
              [("look (!nonexistent)", {})]]

    def __call__(self):
        if 'entity' in self._arguments:
            short = self._arguments['entity'].getDefiniteShort()
            description = self._arguments['entity'].describe()
            self._viewport.display('You look at %s.' % short)
            self._viewport.display(description)
        elif 'nonexistent' in self._arguments:
            self._viewport.display("There's no '%s' to look at." %
                                   self._arguments['nonexistent'])
        else:
            self._viewport.display('You look around.')
            self._viewport.display(self._actor.getRoom().describe())

    def __describe__(self):
        if 'entity' in self._arguments:
            text = 'look at %s' % self._arguments['entity'].getDefiniteShort()
        elif 'nonexistent' in self._arguments:
            text = 'look at %s' % self._arguments['nonexistent']
        else:
            text = 'look'
        return text


class Ponder(Action):
    syntax = [[("ponder", {}),
               ("ponder <!entity>", {})],
              [("ponder (!concept)", {})]]

    def __call__(self):
        if 'entity' in self._arguments:
            text = 'You ponder %s.' % (
                self._arguments['entity'].getDefiniteShort())
        elif 'concept' in self._arguments:
            text = 'You ponder %s.' % self._arguments['concept']
        else:
            text = 'You ponder.'
        self._viewport.display(text)

    def describe(self):
        if 'entity' in self._arguments:
            text = 'ponder %s' % self._arguments['entity'].getDefiniteShort()
        elif 'concept' in self._arguments:
            text = 'ponder %s' % self._arguments['concept']
        else:
            text = 'ponder'
        return text

PLAYER_ACTIONS = [Look, Ponder]
