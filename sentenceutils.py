import re


def string_from_list(items, serial_comma=True):
    first_items = ', '.join(items[:-1])
    final_separator = ', and ' if serial_comma else ' and '

    if len(items) > 1:
        return first_items + final_separator + items[-1]
    else:
        return items[0]


def get_indefinite_article(word):
    return 'an' if word[0].lower() in ['a', 'e', 'i', 'o', 'u'] else 'a'


def sentence_case(phrase):
    result = ''
    for idx, char in enumerate(phrase):
        if char.isspace():
            result += char
        else:
            result += char.upper() + phrase[idx+1:]
            break

    return result


def words(text):
    return [re.sub(r'^[^a-z]*(.*?)[^a-z]*$', r'\1', word)
            for word in text.lower().split()]
