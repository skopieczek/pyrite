import events
import rooms
import things

import functools
import random


class Animate(things.Thing):
    def __init__(self,
                 name,
                 max_health, strength, toughness, speed,
                 intelligence, wisdom,
                 starting_health=None,
                 pronoun='it',
                 visible_adjectives=None, invisible_adjectives=None):

        super(Animate, self).__init__(
            name,
            pronoun=pronoun,
            visible_adjectives=visible_adjectives,
            invisible_adjectives=invisible_adjectives)

        self.max_health = max_health
        self.strength = strength
        self.toughness = toughness
        self.speed = speed
        self.intelligence = intelligence
        self.wisdom = wisdom
        self.health = (max_health if starting_health is None
                       else starting_health)
        self._combats = set()
        self._is_alive = True

        self.listenForEvent(DeathEvent, self.deathEventHandler)

    def __repr__(self):
        return "<Animate: %s>" % self._short

    def onTick(self, tick_event):
        super(Animate, self).onTick(tick_event)

        if self.health <= 0:
            self.die()
            return

        # Attack anyone we're fighting with.
        dead_combatants = []
        for combatant in self._combats:
            if combatant.isAlive():
                self.attack(combatant)
            else:
                dead_combatants.append(combatant)

        # Remove dead enemies from combats list.
        for combatant in dead_combatants:
            self.stopCombat(combatant)

    def startCombat(self, other):
        if isinstance(other, Animate) and other.isAlive():
            self.fireEvent(CombatStarted(self, other))
            self._combats.add(other)

    def stopCombat(self, other):
        if other in self._combats:
            self.fireEvent(CombatStopped(self, other))
            self._combats.remove(other)

    def deathEventHandler(self, death_event):
        if death_event.raiser in self._combats:
            self.stopCombat(death_event.raiser)

    def attack(self, other):
        if self.getRoom() is None or other not in self.getRoom():
            return

        num_hits = max(int(self.speed/other.speed), 1)
        for hit in xrange(num_hits):
            # Damage dealt is equal to the sum of ten random integers between
            # zero and the attacker's strength, divided by ten.
            # This creates a centrally-weighted random number based on strength
            damage = sum((random.random() * self.strength
                         for ii in xrange(10))) / 10.0
            # Adjust for the ratio in attacker's strength to defender's
            # toughness.
            damage *= (self.strength * 1.0 / other.toughness)
            damage = int(damage)

            # There's a 1/10 chance of a miss, multiplied by the ratio of
            # speeds.
            missed_chance = 0.1 * (other.speed * 1.0 / self.speed)

            if random.random() > missed_chance:
                other.getHit(damage, self)
                self.fireEvent(HitEvent(self, other))
            else:
                other.onMissedBy(self)
                self.fireEvent(MissEvent(self, other))

    def getHit(self, damage, aggressor):
        self.health -= damage
        if aggressor not in self._combats:
            self.startCombat(aggressor)

    def onMissedBy(self, other):
        if other not in self._combats:
            self.startCombat(other)

    def isFighting(self):
        return len(self._combats) != 0

    def die(self):
        self._is_alive = False
        self.fireEvent(DeathEvent(self))

        corpse = self.getCorpse()
        if corpse is not None:
            corpse.setRoom(self.getRoom())

        self.delete()

    def isAlive(self):
        return self._is_alive

    def getCorpse(self):
        return Corpse(self)


class Npc(Animate):
    def __init__(self,
                 name,
                 max_health, strength, toughness, speed,
                 intelligence, wisdom,
                 starting_health=None,
                 pronoun='it',
                 visible_adjectives=None, invisible_adjectives=None):

        super(Npc, self).__init__(name,
                                  max_health, strength, toughness, speed,
                                  intelligence, wisdom,
                                  starting_health=starting_health,
                                  pronoun=pronoun,
                                  visible_adjectives=visible_adjectives,
                                  invisible_adjectives=invisible_adjectives)
        self.wander_chance = 0.2
        self.excluded_exits = set()

        self.npc_aggression_chance = 0
        self.player_aggression_chance = 0
        self.pursue_chance = 1
        self.flee_chance = 1
        self.flee_threshold = 0.1

        # Listen for new objects so we can decide whether to attack any new
        # animates in the room.
        self.listenForEvent(rooms.EnteringRoomEvent, self.handleNewObject)
        self.listenForEvent(things.CreatedEvent, self.handleNewObject)

        # Listen for objects leaving the room so we can pursue any fleeing
        # enemies.
        self.listenForEvent(rooms.LeavingRoomEvent, self.handleLeavingEvent)

    def __repr__(self):
        return "<NPC: %s>" % self._short

    # Override
    def onTick(self, tick_event):
        super(Npc, self).onTick(tick_event)

        if not self.isAlive():
            return

        if (self.isFighting() and
                self.health < self.flee_threshold * self.max_health):
            should_flee = False
            room = self.getRoom()
            if room is not None:
                for combatant in self._combats:
                    if combatant in room:
                        should_flee = True
                        break
            if should_flee:
                self.wander()
                return

        if not self.isFighting() and random.random() < self.wander_chance:
            self.wander()
            return

    def wander(self):
        room = self.getRoom()
        if room is not None:
            possible_exits = [exit for exit in room.exits.values()
                              if exit not in self.excluded_exits]
            exit = random.choice(possible_exits)
            self.move(exit)

    def handleNewObject(self, new_object_event):
        if isinstance(new_object_event.raiser, Animate):
            self.maybeFight(new_object_event.raiser)

    def handleLeavingEvent(self, leaving_event):
        # Maybe pursue a fleeing enemy.
        if leaving_event.raiser in self._combats:
            if random.random() < self.pursue_chance:
                if leaving_event.new_room in self.getRoom().exits.values():
                    self.queue(
                        functools.partial(self.move,
                                          new_room=leaving_event.new_room))

    def maybeFight(self, other):
        if other == self:
            return

        if isinstance(other, Npc):
            if random.random() < self.npc_aggression_chance:
                self.startCombat(other)
        else:
            if random.random() < self.player_aggression_chance:
                self.startCombat(other)

    def onEnteringRoom(self):
        room = self.getRoom()
        for animate in room.walkAnimates():
            self.maybeFight(animate)


class Corpse(things.Thing):
    def __init__(self, base_animate):
        super(Corpse, self).__init__("corpse of %s" %
                                     base_animate.getIndefiniteShort())


class DeathEvent(events.Event):
    def __init__(self, raiser):
        super(DeathEvent, self).__init__(raiser)

    def __repr__(self):
        return "<DeathEvent: %s died>" % str(self.raiser)


class CombatEvent(events.Event):
    def __init__(self, raiser, target):
        super(CombatEvent, self).__init__(raiser)
        self.target = target

    def __repr__(self):
        return "<CombatEvent: %s against %s>" % (self.raiser, self.target)


class CombatStarted(CombatEvent):
    def __init__(self, raiser, target):
        super(CombatStarted, self).__init__(raiser, target)

    def __repr__(self):
        return "<CombatStarted: %s attacks %s>" % (self.raiser, self.target)


class CombatStopped(CombatEvent):
    def __init__(self, raiser, target):
        super(CombatStopped, self).__init__(raiser, target)

    def __repr__(self):
        return "<CombatStopped: between %s and %s>" % (
            self.raiser, self.target)


class AttackEvent(CombatEvent):
    def __init__(self, raiser, target):
        super(AttackEvent, self).__init__(raiser, target)

    def __repr__(self):
        return "<AttackEvent: %s attacks %s>" % (self.raiser, self.target)


class HitEvent(AttackEvent):
    def __init__(self, raiser, target):
        super(HitEvent, self).__init__(raiser, target)

    def __repr__(self):
        return "<HitEvent: %s hits %s>" % (self.raiser, self.target)


class MissEvent(AttackEvent):
    def __init__(self, raiser, target):
        super(MissEvent, self).__init__(raiser, target)

    def __repr__(self):
        return "<MissEvent: %s missed %s>" % (self.raiser, self.target)
