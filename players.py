import animates
import actions
import rooms

from sentenceutils import sentence_case


class Player(animates.Animate):
    def __init__(self,
                 name,
                 iolink,
                 max_health, strength, toughness, speed,
                 intelligence, wisdom,
                 starting_health=100,
                 pronoun='he'):
        super(Player, self).__init__(name,
                                     max_health, strength, toughness, speed,
                                     intelligence, wisdom,
                                     starting_health,
                                     pronoun)
        self.viewport = StandardViewPort(iolink, self)

        # TODO - event handlers.

    def setRoom(self, room):
        super(Player, self).setRoom(room)
        self.viewport.setRoom(room)


class ViewPort(object):
    def __init__(self, iolink):
        self._iolink = iolink
        self._recurring_event_handlers = set()
        self._persistent_event_handlers = set()
        self._room = None
        self._actions = set()
        self._iolink.registerInputHandler(self.onInput)

    def setRoom(self, room):
        if self._room is not None:
            for event_type, event_handler in self._recurring_event_handlers:
                self._room.removeEventListener(event_handler, event_type)

        self._room = room

        for event_type, event_handler in self._recurring_event_handlers:
            room.addListener(event_handler, event_type)

        self.display("\n" + room.describe())

    def getParserContext(self):
        return self._room  # Default context is just the room we're viewing.

    def display(self, output):
        self._iolink.handleOutput(output)

    def onInput(self, input_text):
        pass  # Override

    def addAction(self, action):
        self._actions.add(action)

    def removeAction(self, action):
        self._actions.remove(action)

    def listenForEvent(self, event_type, handler):
        self._recurring_event_handlers.add((event_type, handler))
        if self._room is not None:
            self._room.addListener(handler, event_type)

    def stopListeningForEvent(self, event_type, handler):
        self._recurring_event_handlers.remove((event_type, handler))
        if self._room is not None:
            self._room.removeListener(handler, event_type)

    def listenForEventInRoom(self, room, event_type, handler):
        self._persistent_event_handlers.add((room, event_type, handler))
        room.addListener(handler, event_type)

    def stopListeningForEventInRoom(self, room, event_type, handler):
        self._persistent_event_handlers.remove((room, event_type, handler))
        room.removeListener(handler, event_type)


class StandardViewPort(ViewPort):
    def __init__(self, iolink, player):
        super(StandardViewPort, self).__init__(iolink)
        self._player = player
        self.listenForEvent(rooms.EnteringRoomEvent, self.handleEvent)
        self.listenForEvent(rooms.LeavingRoomEvent, self.handleEvent)
        self.listenForEvent(animates.DeathEvent, self.handleEvent)
        self.listenForEvent(animates.HitEvent, self.handleEvent)
        self.listenForEvent(animates.MissEvent, self.handleEvent)
        for action in actions.PLAYER_ACTIONS:
            self.addAction(action)

    def handleEvent(self, event):
        raiser_name = sentence_case(event.raiser.getDefiniteShort())
        if isinstance(event, rooms.EnteringRoomEvent):
            if event.raiser == self._player:
                self.display("\n%s" % event.new_room.describe())
            else:
                self.display("%s arrives." % raiser_name)
        elif isinstance(event, rooms.LeavingRoomEvent):
            if not event.raiser == self._player:
                self.display("%s leaves." % raiser_name)
        elif isinstance(event, animates.DeathEvent):
            if event.raiser == self._player:
                self.display("\nYou are dead.")
            else:
                self.display("%s dies." % raiser_name)
        elif isinstance(event, animates.HitEvent):
            target_name = event.target.getDefiniteShort()
            if event.raiser == self._player:
                self.display("You hit %s." % target_name)
            elif event.target == self._player:
                self.display("%s hits you." % raiser_name)
            else:
                self.display("%s hits %s." % (raiser_name, target_name))
        elif isinstance(event, animates.MissEvent):
            target_name = event.target.getDefiniteShort()
            if event.raiser == self._player:
                self.display("You swing at %s, but miss." % target_name)
            elif event.target == self._player:
                self.display("%s swings at you, but misses." % raiser_name)
            else:
                self.display("%s swings at %s, but misses." %
                             (target_name, raiser_name))

    def onInput(self, input_text):
        input_text = input_text.strip()
        if len(input_text) == 0:
            return

        interpretations = []
        context = self.getParserContext()
        for action in self._actions:
            interpretations.extend(
                action.parse(input_text, self, context, self._player))

        if len(interpretations) == 0:
            self.display("I didn't understand that instruction.")
        elif len(interpretations) == 1:
            # Execute the action.
            interpretations[0]()
        else:
            meanings = [interpretation.describe()
                        for interpretation in interpretations]
            disambiguation = 'Did you mean:\n\t' + '\n\t'.join(meanings)
            self.display(disambiguation)
