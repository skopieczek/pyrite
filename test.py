import time

import animates
import connections
import events
import rooms
import things

INTERESTING_EVENTS = []

# Make some test rooms.
room1 = rooms.Room(description="It's an unremarkable room except for the " +
                               "fact that it's entirely decorated in shades " +
                               "of electric pink.")
room2 = rooms.Room(description="If anything, this room is more pink than the" +
                               " last!")
room1.exits['north'] = room2
room2.exits['south'] = room1


# Make some test objects.
gazelle = animates.Npc('gazelle',
                       40, 5, 15, 25, 20, 18,
                       pronoun='it', visible_adjectives=['anxious'])
gazelle.wander_chance = 0.5

tiger = animates.Npc('tiger',
                     200, 50, 30, 25, 20, 18,
                     pronoun='it',
                     visible_adjectives=['large', 'striped'],
                     invisible_adjectives=['big'])
tiger.wander_chance = 0
tiger.npc_aggression_chance = 1

lion = animates.Npc('lion',
                    200, 50, 30, 25, 20, 18,
                    pronoun='it', visible_adjectives=['fearsome'])
lion.wander_chance = 0
lion.npc_aggression_chance = 1

satchel = things.Container('satchel', 10,
                           visible_adjectives=['brown', 'leather'])
beanbag = things.Thing('beanbag', visible_adjectives=['squishy'])
sack = things.Container('sack', 10, visible_adjectives=['old', 'patched'])
sack.add(satchel)

# Place them in the room.
items = [tiger, lion, gazelle, satchel, beanbag, sack]
for item in items:
    item.setRoom(room1)


def getTestRoom():
    return room1


def startSimulation():
    room_list = [room1, room2]
    connections.ServerThread(5060, connections.PlayerConnectionHandler).start()

    while True:
        time.sleep(5)
        if len(list(room1.walkPlayers())) + len(list(room2.walkPlayers())) > 0:
            print "Found players. Doing stuff."
            for room in room_list:
                room.dispatchEvent(events.TimerTickEvent())
        else:
            print "No players - do nothing."

if __name__ == "__main__":
    startSimulation()
