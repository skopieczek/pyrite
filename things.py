import re

import events
import rooms
from sentenceutils import get_indefinite_article, string_from_list


class Thing(events.EventGenerator):
    DESCRIPTION_ORDER = 0

    def __init__(self, name, pronoun='it',
                 visible_adjectives=None,
                 invisible_adjectives=None):

        if visible_adjectives is None:
            visible_adjectives = []

        if invisible_adjectives is None:
            invisible_adjectives = []

        self._name = name
        self._visible_adjectives = visible_adjectives
        self._invisible_adjectives = invisible_adjectives
        self.pronoun = pronoun

        self.is_proper_noun = False

        self.resetShort()
        self.resetLong()
        self._descriptionComponents = [(Thing.DESCRIPTION_ORDER, self.getLong)]

        self.size = 1.0

        self._room = None

        self._recurring_event_handlers = []
        self._persistent_event_handlers = []
        self._action_queue = []

        self.listenForEvent(events.TimerTickEvent, self.onTick)

    def __str__(self):
        return self._short

    def __repr__(self):
        return "<Thing: %s>" % self._short

    def onTick(self, tick_event):
        for action in self._action_queue:
            action()
        self._action_queue = []

    def queue(self, action):
        self._action_queue.append(action)

    def setRoom(self, room):
        self.bind(room)
        old_room = self._room
        self._room = room

        if old_room is None:
            self.fireEvent(CreatedEvent(self))
        else:
            old_room.remove(self)

        for (event, handler) in self._recurring_event_handlers:
            if old_room is not None:
                old_room.removeListener(handler, event)
            self._room.addListener(handler, event)

        room.add(self)

        self.onEnteringRoom()

    def getRoom(self):
        return self._room

    def move(self, new_room):
        old_room = self._room
        self.fireEvent(rooms.LeavingRoomEvent(self, old_room, new_room))
        self.setRoom(new_room)
        if isinstance(self, Container):
            for item in self.walkContents(recursive=False):
                item.move(new_room)
        self.fireEvent(rooms.EnteringRoomEvent(self, old_room, new_room))

    def onEnteringRoom(self):
        pass

    def delete(self):
        self.fireEvent(DestroyedEvent(self))
        self.bind(None)  # Stop firing events

        room = self._room
        if room is not None:
            room.remove(self)
            for (event, handler) in self._recurring_event_handlers:
                room.removeListener(handler, event)

        for (room, event, handler) in self._persistent_event_handlers:
            room.removeListener(handler, event)

    def setShort(self, short_text):
        self._short = short_text
        self._custom_short = True

    def getShort(self):
        return self._short

    def resetShort(self):
        self._short = ' '.join(self._visible_adjectives)
        if len(self._short) > 0:
            self._short += ' '
        self._short += self._name
        self._custom_short = False

    def setLong(self, long_text):
        self._long = long_text
        self._custom_long = True

    def getLong(self):
        return self._long

    def resetLong(self):
        self._long = "%s's %s." % (self.pronoun.title(),
                                   self.getIndefiniteShort())
        self._custom_long = False

    def describe(self):
        result = ''
        for component in sorted(self._descriptionComponents):
            result += '%s\n' % component[1]()
        component = component[:-1]  # Strip ending newline

        return result

    def isValidIdentifier(self, text):
        text = text.strip()
        if (not self.is_proper_noun and
                (text.startswith('a ') or
                 text.startswith('an ') or
                 text.startswith('the '))):
            text = re.sub(r'.*? (.*)', r'\1', text)
        if not text.endswith(self._name.lower()):
            return False
        else:
            text = text[:len(text)-len(self._name)]
            text = text.strip()
            text = text.replace(',', '')
            adjectives = text.split()
            for adjective in adjectives:
                if not (adjective in self._visible_adjectives or
                        adjective in self._invisible_adjectives):
                    return False
            return True

    def getIndefiniteShort(self):
        if self.is_proper_noun:
            return self._short_description
        else:
            return "%s %s" % (get_indefinite_article(self._short), self._short)

    def getDefiniteShort(self):
        if self.is_proper_noun:
            return self._short_description
        else:
            return "the %s" % self.getShort()

    def listenForEvent(self, event_type, handler):
        self._recurring_event_handlers.append((event_type, handler))
        if self._room is not None:
            self._room.addListener(handler, event_type)

    def stopListeningForEvent(self, event_type, handler):
        self._recurring_event_handlers.remove((event_type, handler))
        if self._room is not None:
            self._room.removeListener(handler, event_type)

    def listenForEventInRoom(self, room, event_type, handler):
        self._persistent_event_handlers.append((room, event_type, handler))
        room.addListener(handler, event_type)

    def stopListeningForEventInRoom(self, room, event_type, handler):
        self._persistent_event_handlers.remove((room, event_type, handler))
        room.removeListener(handler, event_type)


class Container(Thing):
    DESCRIPTION_ORDER = 10

    def __init__(self, name, capacity, pronoun='it',
                 visible_adjectives=None, invisible_adjectives=None):

        super(Container, self).__init__(
            name,
            pronoun=pronoun,
            visible_adjectives=visible_adjectives,
            invisible_adjectives=invisible_adjectives)
        self.capacity = capacity
        self._capacity_left = capacity
        self._contents = []
        self._descriptionComponents.append((Container.DESCRIPTION_ORDER,
                                            self.getContentDescription))

    def add(self, item):
        if item in self:
            raise Container.AlreadyContainsItemError(self, item)
        elif item.size > self.capacity:
            raise Container.NotEnoughRoomError(self, item)
        elif item == self:
            raise Container.CannotAddItemToItselfError(item)
        elif (isinstance(item, Container) and
              self in item.walkContents(recursive=True)):
            raise Container.CircularContentsException(self, item)
        else:
            self._contents.append(item)
            self._capacity_left -= item.size
            self.fireEvent(ItemAddedEvent(self, item))

    def remove(self, item):
        try:
            self._contents.remove(item)
            self._capacity_left += item.size
            self.fireEvent(ItemRemovedEvent(self, item))
        except ValueError:
            raise Container.NoSuchItemError(self, item)

    def getContentDescription(self):
        if self.isEmpty():
            return 'It is currently empty.'
        else:
            contents_list = string_from_list([item.getIndefiniteShort()
                                              for item in self._contents])
            return '%s contains %s.' % (self.pronoun.title(), contents_list)

    def __contains__(self, item):
        return item in self._contents

    def isEmpty(self):
        return len(self._contents) == 0

    def walkContents(self, recursive=False):
        for item in self._contents:
            yield item
            if recursive and isinstance(item, Container):
                for subitem in item.walkContents(recursive=True):
                    yield subitem

    class AlreadyContainsItemError(Exception):
        def __init__(self, container, item):
            message = '%s already contains %s.' % (
                container.getDefiniteShort(),
                item.getDefiniteShort())
            Exception.__init__(self, message)
            self.container = container
            self.item = item

    class NoSuchItemError(Exception):
        def __init__(self, container, item):
            message = '%s does not contain %s.' % (
                container.getDefiniteShort(),
                item.getDefiniteShort())
            Exception.__init__(self, message)
            self._container = container
            self._item = item

    class NotEnoughRoomError(Exception):
        def __init__(self, container, item):
            message = 'Not enough room for %s in %s.' % (
                container.getDefiniteShort(),
                item.getDefiniteShort())
            Exception.__init__(self, message)
            self._container = container
            self._item = item

    class CannotAddItemToItselfError(Exception):
        def __init__(self, item):
            message = 'Cannot add %s to itself.' % item.getDefiniteShort()
            Exception.__init__(self,  message)
            self._item = item

    class CircularContentsException(Exception):
        def __init__(self, container, item):
            item_string = item.getDefiniteShort()
            container_string = container.getDefiniteShort()
            message = ('Adding %s to %s would create a contents loop: %s is' +
                       ' an ultimate parent of %s.' % (
                           item_string, container_string,
                           item_string, container_string))
            Exception.__init__(self, message)
            self._container = container
            self._item = item


class CreatedEvent(events.Event):
    def __init__(self, raiser):
        super(CreatedEvent, self).__init__(raiser)

    def __repr__(self):
        return "<CreatedEvent: '%s'>" % str(self.raiser)


class DestroyedEvent(events.Event):
    def __init__(self, raiser):
        super(DestroyedEvent, self).__init__(raiser)

    def __repr__(self):
        return "<DestroyedEvent: '%s'>" % str(self.raiser)


class ItemAddedEvent(events.Event):
    def __init__(self, raiser, subitem):
        super(ItemAddedEvent, self).__init__(raiser)
        self.subitem = subitem

    def __repr__(self):
        return "<ItemAddedEvent: '%s' into '%s'>" % (
            str(self.subitem), str(self.raiser))


class ItemRemovedEvent(events.Event):
    def __init__(self, raiser, subitem):
        super(ItemRemovedEvent, self).__init__(raiser)
        self.subitem = subitem

    def __repr__(self):
        return "<ItemRemovedEvent: '%s' from '%s'>" % (
            str(self.subitem), str(self.raiser))
