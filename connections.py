import re
import socket
import threading

import accounts
import worldmanager


class ServerThread(threading.Thread):
    def __init__(self, port, connection_handler_class, max_connections=10):
        super(ServerThread, self).__init__()
        self._port = port
        self._max_connections = max_connections
        self._connection_handler_class = connection_handler_class

    def run(self):
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.bind(('', self._port))
        server.listen(self._max_connections)
        while True:
            client, address = server.accept()
            connection = Connection(client)
            self._connection_handler_class(connection).start()


class Connection(object):
    BUFSIZE = 1024

    def __init__(self, client):
        self._client = client
        self._recv_backlog = []
        self._recv_in_progress = ""
        self.connected = True

    def sendline(self, msg):
        self.send(msg + "\n")

    def send(self, msg):
        msg = re.sub("(?<!\r)\n", "\r\n", msg)
        while len(msg) > 0:
            sent = self._client.send(msg)
            if sent == 0:
                self.on_disconnect()
                break
            else:
                msg = msg[sent:]

    def recvline(self):
        if len(self._recv_backlog) > 0:
            result = self._recv_backlog[0]
            self._recv_backlog = self._recv_backlog[1:]
            return result
        elif not self.connected:
            return None
        else:
            transmission = self._recv_in_progress
            while True:
                chunk = self._client.recv(Connection.BUFSIZE)
                if chunk == '':
                    self.disconnect()
                    return chunk + "\n"
                else:
                    transmission += chunk
                    if '\n' in transmission:
                        lines = [line.strip()
                                 for line in transmission.split('\n')]
                        self._recv_backlog.extend(lines[1:-1])
                        if '\n' in lines[-1]:
                            self._recv_backlog.append(lines[-1])
                        else:
                            self._recv_in_progress = lines[-1]
                        return lines[0]

    def disconnect(self):
        self.connected = False
        self._client.close()


class AbstractConnectionHandler(threading.Thread):
    def __init__(self, connection):
        super(AbstractConnectionHandler, self).__init__()
        self._connection = connection

    def run(self):
        self.onConnect()

        while self._connection.connected:
            self.execMainLoop()

        self.onDisconnect()

    def onConnect(self):
        pass

    def onDisconnect(self):
        pass

    def execMainLoop(self):
        pass


class PlayerConnectionHandler(AbstractConnectionHandler):
    def __init__(self, connection):
        super(PlayerConnectionHandler, self).__init__(connection)
        self._player = None
        self._iolink = None

    def onConnect(self):
        self._iolink = TelnetIOLink(self._connection)
        success = self._doLogin()
        if not success:
            self._connection.sendline("Goodbye.")
            self._connection.disconnect()

    def execMainLoop(self):
        command = self._connection.recvline()
        self._iolink.handleInput(command)

    def _doLogin(self):
        assert(self._player is None)  # TODO Use proper logging

        self._connection.sendline("Welcome to %s." % worldmanager.world_name)
        self._connection.sendline("\nPress ENTER to begin.")
        self._connection.recvline()
        self._connection.sendline("Please enter your details.")

        attempts_left = 3
        while attempts_left > 0:
            self._connection.send("Username: ")
            username = self._connection.recvline()
            self._connection.send("Password: ")
            password = self._connection.recvline()

            self._player = accounts.login(username, password, self._iolink)

            if self._player is not None:
                break

            self._connection.sendline("\r\nDetails not recognised. Try again.")
            attempts_left -= 1

        return (self._player is not None)


class IOLink(object):
    def __init__(self):
        self._input_handlers = set()

    def registerInputHandler(self, handler):
        self._input_handlers.add(handler)

    def removeInputHandler(self, handler):
        self._input_handler.remove(handler)

    def handleInput(self, input_text):
        for handler in self._input_handlers:
            handler(input_text)

    def handleOutput(self, output_text):
        pass  # Override


class TerminalLink(IOLink):
    def __init__(self):
        super(TerminalLink, self).__init__()

    def beginLoop(self):
        while True:
            input_text = raw_input("")
            self.handleInput(input_text)

    def handleOutput(self, output_text):
        print output_text


class TelnetIOLink(IOLink):
    def __init__(self, connection):
        super(TelnetIOLink, self).__init__()
        self.connection = connection

    def handleOutput(self, output_text):
        if self.connection.connected:
            self.connection.sendline(output_text)
