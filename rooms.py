import itertools

import animates
import commandparser
import events
import sentenceutils


class Room(events.EventProxy, commandparser.ParserContext):
    """Represents an area in which people and objects can be colocated.
       Rooms also pass on ticks from the main event queue to their contents.
       A room has 8 neighbour rooms, one at each compass point, unless the room
       is at a corner or edge of the map.
       The interface between a Room and its neighbour room is an Exit, which
       may or may not allow characters to pass through."""
    def __init__(self, description="Nothing to see here.", exits=None):
        events.EventProxy.__init__(self)
        self.description = description
        self.exits = {} if exits is None else exits
        self._inanimates = set()
        self._npcs = set()
        self._players = set()

    def describe(self):
        description = self.description
        contents = list(self.walkContents())
        if len(contents) == 1:
            description += '\n\n%s is here.' % (
                contents[0].getIndefiniteShort().title())
        elif len(contents) > 1:
            description += sentenceutils.sentence_case(
                '\n\n%s are here.' % (
                    sentenceutils.string_from_list(
                        [item.getIndefiniteShort() for item in contents]))
                )

        exits = list(self.exits)
        if len(exits) == 0:
            description += '\n\nThere are no obvious exits.'
        elif len(exits) == 1:
            description += '\n\nThe only obvious exit is %s.' % exits[0]
        else:
            description += ('\n\nExits are %s.' %
                            sentenceutils.string_from_list(exits))

        return description

    def add(self, item):
        if isinstance(item, animates.Npc):
            self._npcs.add(item)
        else:
            self._inanimates.add(item)

    def remove(self, item):
        if isinstance(item, animates.Npc):
            self._npcs.remove(item)
        else:
            self._inanimates.remove(item)

    def __contains__(self, item):
        return (item in self._inanimates or
                item in self._npcs or
                item in self._players)

    def walkInanimates(self):
        return iter(self._inanimates)

    def walkNpcs(self):
        return iter(self._npcs)

    def walkPlayers(self):
        return iter(self._players)

    def walkAnimates(self):
        return itertools.chain(self.walkNpcs(), self.walkPlayers())

    def walkContents(self):
        return itertools.chain(self.walkInanimates(),
                               self.walkNpcs(),
                               self.walkPlayers())

    # ParserContext methods
    def getEntities(self):
        return list(self.walkContents())

    def hasEntity(self, entity_text):
        for entity in self.walkContents():
            if entity.isValidIdentifier(entity_text):
                return True
        return False

    def getEntity(self, entity_text):
        for entity in self.walkContents():
            if entity.isValidIdentifier(entity_text):
                return entity
        return None

    def say(self, text):
        # TODO use a viewport of some kind.
        print "[%s]: %s" % (str(self), text)


class Exit(object):
    """An Exit represents the interface between a Room and one of its
       neighbours. A character may or may not be able to see or pass through
       it, and it may or may not be listed in the room description."""

    def __init__(self, is_open):
        self._is_open = is_open
        self._obviousness = None
        self._transparency = None
        self._blocked_message = "You cannot go that way."

    def setIsOpen(self, is_open):
        """Set whether or not characters can pass through the exit, according to
           is_open, which should be True or False."""
        self._open = is_open

    def isOpen(self, is_open):
        """Returns True if characters can pass through the exit; False
          otherwise."""
        return self._open

    def setObviousness(self, is_obvious):
        """Set whether or not the exit is listed in the description of the
           room, according to the value of is_obvious.
           If is_obvious is True, the exit will be listed irrespective of its
           other properties, even if it is Closed.
           If is_obvious is False, the exit will never be listed, even if open.
           If is_obvious is None, the exit will be listed if and only if it is
           open."""
        self._obviousness = is_obvious

    def isObvious(self):
        """Returns True if the exit will be listed in the description of the
           room, and False otherwise."""
        if self._obviousness is not None:
            return self._obviousness
        else:
            # When self._obvious is None, that indicates that the exit is
            # visible if and only if it is open.
            return self._open

    def isTransparent(self, is_transparent):
        """Returns True if the next room can be seen through the exit, and False
           otherwise."""
        if self._is_transparent is not None:
            return self._is_transparent
        else:
            return self.isObvious()

    def getBlockedMessage(self):
        """Get the message shown if a player tries to pass through the exit and
           it is blocked."""
        return self._blocked_message

    def setBlockedMessage(self, message):
        """Set the message shown if a player tries to pass through the exit and
           it is blocked."""
        self._blocked_message = message


class MovementEvent(events.Event):
    def __init__(self, raiser):
        super(MovementEvent, self).__init__(raiser)


class EnteringRoomEvent(MovementEvent):
    def __init__(self, raiser, new_room, previous_room):
        # Note that previous_room may be None.
        super(EnteringRoomEvent, self).__init__(raiser)
        self.new_room = new_room
        self.previous_room = previous_room

    def __repr__(self):
        return ("<EnteringRoomEvent: %s enters %s from %s>" %
                (self.raiser, self.previous_room, self.new_room))


class LeavingRoomEvent(MovementEvent):
    def __init__(self, raiser, previous_room, new_room):
        # Note that previous_room may be None.
        super(LeavingRoomEvent, self).__init__(raiser)
        self.new_room = new_room
        self.previous_room = previous_room

    def __repr__(self):
        return "<LeavingRoomEvent: %s leaves %s for %s>" % (self.raiser,
                                                            self.previous_room,
                                                            self.new_room)
